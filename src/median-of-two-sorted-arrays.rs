/// https://leetcode.com/problems/median-of-two-sorted-arrays/
/// There are two sorted arrays nums1 and nums2 of size m and n respectively.
///
/// Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
/// You may assume nums1 and nums2 cannot be both empty.
///
/// Example 1:
/// 	nums1 = [1, 3]
/// 	nums2 = [2]
///
///     The median is 2.0
/// 
/// Example 2:
/// 	nums1 = [1, 2]
/// 	nums2 = [3, 4]
///
///     The median is (2 + 3)/2 = 2.5

struct Solution;

impl Solution {
    // pub fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
    // 	let (n, m) = match nums1.len() < nums2.len() {
    // 		true => (nums1.len(), nums2.len()),
    // 		_	 => (nums2.len(), nums1.len()),
    // 	};

    // 	let mut median = 0f64;
    // 	let mut min_index = 0;
    // 	let mut max_index = n;
    // 	let mut i = 0;
    // 	let mut j = 0;

    // 	'med : while min_index < max_index {
    // 		i = (min_index + max_index) / 2;
    // 		j = (n + m + 1) / 2 - i;

    // 		if i < n && j > 0 && nums2[j - 1] > nums1[i] {
    // 			min_index = i + 1;
    // 		} else if i > 0 && j < m && nums2[j] < nums1[i - 1] {
    // 			min_index = i - 1;
    // 		} else {
    // 			median = if i == 0 {
    // 				nums2[j - 1] as f64
    // 			} else if j == 0 {
    // 				nums1[i - 1] as f64
    // 			} else {
    // 				match nums1[i - 1] < nums2[j - 1] {
    // 					true => nums2[j - 1] as f64,
    // 					_ 	 => nums1[i - 1] as f64,
    // 				}
    // 			};

    // 			break 'med;
    // 		}
    // 	}

    // 	if (n + m) % 2 == 0 {
    // 		return median;
    // 	}

    // 	if i == n {
    // 		return (median + nums2[j] as f64) / 2f64;
    // 	}

    // 	if j == m {
    // 		return (median + nums1[i] as f64) / 2f64;
    // 	}

    // 	match nums1[i] < nums2[j] {
    // 		true => return (median + nums1[i] as f64) / 2f64,
    // 		_	 => return (median + nums2[j] as f64) / 2f64,
    // 	}
    // }

    
    fn find_median_sorted_arrays(nums1 : Vec<i32>, nums2 : Vec<i32>) -> f64 {
        let nums1_size = nums1.len();
        let nums2_size = nums2.len();
        
        let size = nums1_size + nums2_size;

        0f64
    }
}

fn main() {
    // println!("{}",
    // Solution::find_median_sorted_arrays(vec![3, 5, 10, 11, 17], vec![9, 13, 20, 21, 23, 27]));
    // println!("Median of [1, 3] and [2] is {}", 
    // 	Solution::find_median_sorted_arrays(vec![3, 5, 10, 11, 17], vec![9, 13, 20, 21, 23, 27]));

    println!("Median of [1, 2] and [3, 4] is {}", 
        Solution::find_median_sorted_arrays(vec![1, 2], vec![3, 4]));
}