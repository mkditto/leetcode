struct Solution;

impl Solution {
    pub fn my_pow(x: f64, n: i32) -> f64 {
        if n == 0 {
            return 1f64;
        } else if x == 0f64 {
            return 0f64;
        }

        std::iter::repeat(())
            .scan(n, |n, _| {
                *n = if *n == 0 { 0 } else if *n < 0 { *n + 1 } else { *n - 1 };
                Some(*n)
            })
            .take_while(|n| *n != 0)
            .fold(if n < 0 { 1f64 / x } else { x }, |r, _| {
                if n < 0 { r * (1f64 / x) } else { r * x }
                // if n % 2 == 1 {
                //     r = (r * x) % 1_000_000_007f64;
                // }

                // n /= 2;

                // x = (x * x) % 1_000_000_007f64;

                // r
            })
    }
}


fn main() {
    // println!("{}", Solution::my_pow(2.0, -2));
    // println!("{}", Solution::my_pow(0.00001, 2147483647));
    println!("{}", Solution::my_pow(2.0, 10));
    // println!("{}", Solution::my_pow(2.0, -10));
}